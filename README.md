# dotnet-core-aspnet-libgdiplus

`mcr.microsoft.com/dotnet/core/aspnet` images with `libgdiplus`

## Overview

Certain features in `.netstandard` that use `System.Drawing` at runtime need access to some libraries such as `libgdiplus`. If these libraries are not available, it will throw a runtime exception similar to this:
```
System.TypeInitializationException : The type initializer for 'Gdip' threw an exception.
---- System.DllNotFoundException : Unable to load DLL 'libgdiplus': The specified module could not be found.
   at System.Drawing.SafeNativeMethods.Gdip.GdipLoadImageFromDelegate_linux(StreamGetHeaderDelegate getHeader, StreamGetBytesDelegate getBytes, StreamPutBytesDelegate putBytes, StreamSeekDelegate doSeek, StreamCloseDelegate close, StreamSizeDelegate size, IntPtr& image)
   at System.Drawing.Image.InitFromStream(Stream stream)
   at System.Drawing.Image.LoadFromStream(Stream stream, Boolean keepAlive)
   at Pompa.Invoicing.Xslt.ImageUtility.Resize(Byte[] image, Int32 width, Int32 height) in /home/j/Projects/pompa/src/Pompa.Invoicing/Xslt/ImageUtility.cs:line 15
   at Pompa.Invoicing.Xslt.XsltGenerator.GetEArchive(Byte[] signature, DiscountType discountType, Byte[] logo) in /home/j/Projects/pompa/src/Pompa.Invoicing/Xslt/XsltGenerator.cs:line 41
   at Pompa.Business.Tests.Gib.FakeInvoiceGeneratorBase.GenerateXslt(Boolean eb, DiscountType discountType) in /home/j/Projects/pompa/src/Pompa.Business.Tests/Gib/FakeInvoiceGeneratorBase.cs:line 137
   at Pompa.Business.Tests.Gib.FakeInvoiceGeneratorBase.GenerateArchiveUbl(DiscountType discountType) in /home/j/Projects/pompa/src/Pompa.Business.Tests/Gib/FakeInvoiceGeneratorBase.cs:line 116
   at Pompa.Business.Tests.Gib.GibProxyFacts`2.CreateArchiveInvoice(DiscountType discountType) in /home/j/Projects/pompa/src/Pompa.Business.Tests/Gib/GibProxyFacts.cs:line 47
   at Pompa.Business.Tests.Gib.GibProxyFacts`2.GetArchivePDF_Should_Get(DiscountType discountType) in /home/j/Projects/pompa/src/Pompa.Business.Tests/Gib/GibProxyFacts.cs:line 111
--- End of stack trace from previous location where exception was thrown ---
----- Inner Stack Trace -----
   at System.Runtime.InteropServices.FunctionWrapper`1.get_Delegate()
   at System.Drawing.SafeNativeMethods.Gdip.GdiplusStartup(IntPtr& token, StartupInput& input, StartupOutput& output)
   at System.Drawing.SafeNativeMethods.Gdip..cctor()
```

The motivation for this project is to create docker images for the official DotNet Core AspNet images (e.g: `mcr.microsoft.com/dotnet/core/aspnet`) with the addition of `libgdiplus` and `libc6-dev` required at runtime.
```
RUN apt-get update \
    && apt-get install -y --no-install-recommends libgdiplus libc6-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
```

## Docker Hub

The images are released in Docker Hub:
https://hub.docker.com/r/sunnyatticsoftware/dotnet-core-aspnet-libgdiplus/tags
